import { createStore , applyMiddleware , compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import rootReducer from './reducers/index';

export default function configureStore(initialState){
    // const logger = createLogger();
    const enhancer= compose(applyMiddleware(thunk,promise));
    return createStore(rootReducer, initialState, enhancer);
}
