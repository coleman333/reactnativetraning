import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NativeRouter, Route, Link } from 'react-router-native'
// import MainPage from "./MainPage";
import MainPage from '../containers/MainPage';
import PageTwo from '../containers/PageTwo';
import PageThree from "../containers/PageThree";
import PageForth from "../containers/PageForth";


const Home = () => (
    <Text style={styles.header}>
        Home
    </Text>
)

const About = () => (
    <Text style={styles.header}>
        About
    </Text>
)

const Topic = ({ match }) => (
    <Text style={styles.topic}>
        {match.params.topicId}
    </Text>
)

const Topics = ({ match }) => (
    <View>
        <Text style={styles.header}>Topics</Text>
        <View>
            <Link
                to={`${match.url}/rendering`}
                style={styles.subNavItem}
                underlayColor='#f0f4f7'>
                <Text>Rendering with React</Text>
            </Link>
            <Link
                to={`${match.url}/components`}
                style={styles.subNavItem}
                underlayColor='#f0f4f7'>
                <Text>Components</Text>
            </Link>
            <Link
                to={`${match.url}/props-v-state`}
                style={styles.subNavItem}
                underlayColor='#f0f4f7'>
                <Text>Props v. State</Text>
            </Link>
        </View>

        <Route path={`${match.url}/:topicId`} component={Topic}/>
        <Route exact path={match.url} render={() => (
            <Text style={styles.topic}>Please select a topic.</Text>
        )} />
    </View>
)

const App2 = () => (
    <NativeRouter>
        <View style={styles.container}>
            <View style={styles.nav}>
                <Link
                    to="/"
                    underlayColor='#f0f4f7'
                    style={styles.navItem}>
                    <Text>Home</Text>
                </Link>
                <Link
                    to="/pageOne"
                    underlayColor='#f0f4f7'
                    style={styles.navItem}>
                    <Text>ONE</Text>
                </Link>
                <Link
                    to="/pageTwo"
                    underlayColor='#f0f4f7'
                    style={styles.navItem}>
                    <Text>TWO</Text>
                </Link>
                <Link
                    to="/pageThree"
                    underlayColor='#f0f4f7'
                    style={styles.navItem}>
                    <Text>THREE</Text>
                </Link>
                <Link
                    to="/pageForth"
                    underlayColor='#f0f4f7'
                    style={styles.navItem}>
                    <Text>FORTH</Text>
                </Link>
                <Link
                    to="/topics"
                    underlayColor='#f0f4f7'
                    style={styles.navItem} >
                    <Text>Topics</Text>
                </Link>

            </View>

            <Route exact path="/" component={Home}/>
            <Route path="/pageOne" component={MainPage}/>
            <Route path="/pageTwo" component={PageTwo}/>
            <Route path="/pageThree" component={PageThree}/>
            <Route path="/pageForth" component={PageForth}/>
            <Route path="/topics" component={Topics}/>

            {/*<Text>HELLO </Text>*/}
        </View>
    </NativeRouter>
)

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        padding: 10,
    },
    header: {
        fontSize: 20,
    },
    nav: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    navItem: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    subNavItem: {
        padding: 5,
    },
    topic: {
        textAlign: 'center',
        fontSize: 15,
    }
})

export default App2