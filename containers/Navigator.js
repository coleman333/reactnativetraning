import { createStackNavigator,} from 'react-navigation';
import MainPage from '../containers/MainPage';
import PageOne from '../containers/PageOne';
import PageForth from '../containers/PageForth';


const Navigator = createStackNavigator({
    PageOne: { screen: PageOne },
    MainPage: { screen: MainPage },
    PageForth: { screen: PageForth },

}, {initialRouteName: 'MainPage'});

export default Navigator;

