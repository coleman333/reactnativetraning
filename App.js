import React from 'react';
import { AppRegistry, StyleSheet, Text, Button, View, TextInput , Slider ,Image } from 'react-native';
import f from "./images/Facebook.png"
import  userAction from './actions/userAction';
import {bindActionCreators} from 'redux';
import { Provider } from "react-redux";
// import RouterWithRedux = connect()(Router)
import MainPage from './containers/MainPage';
import PageOne from './containers/PageOne';
import configureStore from './configureStore';
import PageTwo from "./containers/PageTwo";
import Navigator from "./containers/Navigator";
import PageReactNavigator from "./containers/PageReactNavigator";
const store = configureStore();

import { createStackNavigator,} from 'react-navigation';


// const Navigator = createStackNavigator({
//     PageOne: { screen: PageOne },
//     MainPage: { screen: MainPage },
//
// }, {initialRouteName: 'MainPage'});


export default class App extends React.Component {
  state={
        arrNewObject:[
            {
                id: 1,
                partnerName: 'partner1',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 2,
                partnerName: 'partner2',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 3,
                partnerName: 'partner3',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'приостановлена'
            },
            {
                id: 4,
                partnerName: 'partner4',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 5,
                partnerName: 'partner5',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'приостановлена'
            },
            {
                id: 6,
                partnerName: 'partner6',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 7,
                partnerName: 'partner7',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 8,
                partnerName: 'partner8',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'приостановлена'
            },
            {
                id: 6,
                partnerName: 'partner6',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 7,
                partnerName: 'partner7',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'новая'
            },
            {
                id: 8,
                partnerName: 'partner8',
                date: '23 мая 2018 10:52',
                bonusRequest: '12345',
                status: 'приостановлена'
            }
        ]

    };

  render() {
    return (
        <Provider store={store}>
            {/*<PageOne/>*/}
            <Navigator/>
            {/*{children}*/}
        </Provider>
    );
  }
}


AppRegistry.registerComponent('App', () => App);