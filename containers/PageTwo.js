import React from 'react';
import { StyleSheet, Text, Button, View, TextInput , Slider  ,ToolbarAndroid ,DrawerLayoutAndroid
    ,TouchableOpacity,TouchableHighlight ,FlatList} from 'react-native';
import Image from 'react-native-remote-svg'
import f from "../images/Facebook.png"
import camera from "../images/camera.svg"
import  userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import { connect } from "react-redux";
import { NativeRouter, Route, Link } from 'react-router-native'
import PageOne from '../containers/PageOne'
import PageThree from '../containers/PageThree'

import { withRouter } from 'react-router-dom'
import MainPage from "./MainPage";

class PageTwo extends React.Component {
    componentDidMount(){
        this.props.userAction.getTestUsers();
        // this.props.userAction.getGists();
    }

    redirectTo(){
        console.log('this is redirect to page two', this.props.history);
        this.props.history.push('/MainPage`');
    }

    redirectToPageOne(){
        const { navigate } = this.props.navigation;
        console.log('this is redirect ot one')
        navigate('PageThree');
    }

    // onActionSelected: function(position) {
    //     if (position === 0) { // index of 'Settings'
    //         showSettings();
    //     }
    // }

    // navigationView = (
    //     <View style={{flex: 1, backgroundColor: '#fff'}}>
    //         <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>I'm in the Drawer!</Text>
    //     </View>
    // );

    // navigate() {
    //     this.props.navigator.push({ component: PageTwo });
    // }

    render() {
        console.log('RENDER!');

        return (
            <View style={styles.container}>
                <ToolbarAndroid
                    logo={require('../images/Facebook.png')}
                    title="AwesomeApp"
                    actions={[{title: 'Settings', icon: require('../images/Facebook.png'), show: 'always'}]}
                    // onActionSelected={this.onActionSelected}
                />
                <Text>Open up App.js to start working on your app!</Text>
                <Text style={textStyle.font}>Hello</Text>
                <Button style={buttonStyle.view} title={'redirect on page two'} onPress={this.redirectTo.bind(this)}/>
                <Button style={buttonStyle.view} title={'second button redirect on page two'} />

                {/*{ this.props.gists && this.props.gists.map(user => {*/}
                {/*return <Text>{user.description}</Text>*/}
                {/*})}*/}
                {/*{ this.props.users && this.props.users.map(user => {*/}
                {/*return <Text>{user.email}</Text>*/}
                {/*})}*/}
                <TouchableOpacity onPress={this.redirectTo.bind(this)}>
                    <Image source={f} style={{width: 50, height: 50}}/>
                </TouchableOpacity>

                <TouchableHighlight  style={buttonStyle.view} >
                    <View>
                        <Image source={camera} style={{width:20,position:'relative',left:-30,
                            top:10,backgroundColor:'white'}}/>
                        <Text style={{color: 'white',position:'relative', bottom:15}}> Touch Here </Text>
                    </View>

                </TouchableHighlight>

                <TouchableOpacity onPress={this.redirectToPageOne.bind(this)} style={{
                    borderWidth:1,
                    // borderColor:'rgba(0,0,0,0.2)',
                    borderColor:'black',
                    alignItems:'center',
                    justifyContent:'center',
                    width:100,
                    height:100,
                    backgroundColor:'black',
                    borderRadius:100,

                }}
                >
                    <Image source={camera} style={{width: 50, height: 50}} />
                </TouchableOpacity>

                {/*<DrawerLayoutAndroid*/}
                    {/*drawerWidth={300}*/}
                    {/*drawerBackgroundColor="gray"*/}
                    {/*drawerPosition={DrawerLayoutAndroid.positions.Left}*/}
                    {/*renderNavigationView={() => this.navigationView}>*/}
                    {/*<View style={{flex: 1, alignItems: 'center'}}>*/}
                        {/*<Text style={{margin: 10, fontSize: 15, textAlign: 'right'}}>Hello</Text>*/}
                        {/*<Text style={{margin: 10, fontSize: 15, textAlign: 'right'}}>World!</Text>*/}
                    {/*</View>*/}
                {/*</DrawerLayoutAndroid>*/}

                <FlatList
                    data={this.props.allUsers}
                    renderItem={({item}) => <Text>{item.email}</Text>}
                />
                {/*<Button*/}
                    {/*title="Go to Jane's profile"*/}
                    {/*onPress={ this.navigate() }*/}

                {/*/>*/}
            </View>


        );
    }
}



const styles = StyleSheet.create({
    container: {
        display: 'flex',
        width: "100%",
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
        height: 400
    },
});

const textStyle = StyleSheet.create({
    font: {
        color: 'green',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


const buttonStyle = StyleSheet.create({
    view: {
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        width:200,
        height:50
    },
});


const mapStateToProps = (state) => {
    return {
        users: state.userReducer.allUsers,
        gists: state.userReducer.gists
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PageTwo);
