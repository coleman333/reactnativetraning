import React from 'react';
import {StyleSheet, Text, Button, View, TextInput, Slider, Image, TouchableOpacity} from 'react-native';
import f from "../images/Facebook.png"
import  userAction from '../actions/userAction';
import {bindActionCreators} from 'redux';
import { connect } from "react-redux";
// import {Router, browserHistory} from 'react-router';
// import { BrowserRouter as Router, Link} from 'react-router-dom';
// import { NativeRouter, Route, Link } from 'react-router-native'
import { NativeRouter, Route, Link } from 'react-router-native'
import { withRouter } from 'react-router-dom'

class MainPage extends React.Component {
    componentDidMount(){
        this.props.userAction.getTestUsers();
        this.props.userAction.getGists();
    }

    onPress() {
        const { navigate } = this.props.navigation;
        console.log(navigate);
        navigate('PageOne');
    }

    redir(){
        const {navigate} = this.props.navigation;
        navigate('PageForth')
    }

    render() {
        console.log('RENDER!');
        return (
            <View style={styles.container}>
                <Text>Open up App.js to start working on your app!</Text>
                <Text style={textStyle.font}>Hello</Text>
                <Button title={'redirect on page one'} onPress={this.onPress.bind(this)}/>

                {/*{ this.props.gists && this.props.gists.map(user => {*/}
                    {/*return <Text>{user.description}</Text>*/}
                {/*})}*/}
                {/*{ this.props.users && this.props.users.map(user => {*/}
                    {/*return <Text>{user.email}</Text>*/}
                {/*})}*/}
                <TouchableOpacity onPress={this.redir.bind(this)} style={{
                    borderWidth:1,
                    // borderColor:'rgba(0,0,0,0.2)',
                    borderColor:'black',
                    alignItems:'center',
                    justifyContent:'center',
                    width:100,
                    height:100,
                    backgroundColor:'black',
                    borderRadius:100,
                }}
                >
                    <Image source={f} style={{width: 40, height: 40}}/>
                </TouchableOpacity>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        display: 'flex',
        width: "100%",
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center'
    },
});

const textStyle = StyleSheet.create({
    font: {
        color: 'green',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


const mapStateToProps = (state) => {
    return {
        users: state.userReducer.allUsers,
        gists: state.userReducer.gists
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userAction: bindActionCreators(userAction, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
