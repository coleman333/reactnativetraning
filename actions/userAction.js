import { createActionAsync } from "redux-act-async";
import axios from "axios";
// import { createAction } from "redux-actions";


export default {
    getTestUsers: createActionAsync('GET_TEST_USERS', () => {

        return axios({
            method: 'get',
            url: `https://jsonplaceholder.typicode.com/users`,
        });
    }, {rethrow: true}),
    getGists: createActionAsync('GET_GISTS', () => {

        return axios({
            method: 'get',
            url: `https://api.github.com/gists`,
        });
    }, {rethrow: true}),
};

