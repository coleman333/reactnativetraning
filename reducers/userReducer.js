import cloneDeep from 'lodash/cloneDeep';
import userAction from '../actions/userAction';
import {createReducer} from 'redux-act';

const initialState = {
	processing: 0,
	error: {},
	user: {},
    allUsers: []
};

export default createReducer({
	[userAction.getTestUsers.request]: (state, payload) => {
		const newState = cloneDeep(state);
		return newState;
	},
	[userAction.getTestUsers.ok]: (state, payload) => {
		const newState = cloneDeep(state);
        newState.allUsers = payload.response.data;
        // console.log('this is from react native', newState.allUsers);
		return newState;
	},
	[userAction.getTestUsers.error]: (state, payload) => {
		const newState = cloneDeep(state);

		return newState;
	},

    [userAction.getGists.request]: (state, payload) => {
        const newState = cloneDeep(state);
        return newState;
    },
    [userAction.getGists.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.gists = payload.response.data;
        // console.log('this is from react native', newState.gists);
        return newState;
    },
    [userAction.getGists.error]: (state, payload) => {
        const newState = cloneDeep(state);

        return newState;
    },

}, initialState);
